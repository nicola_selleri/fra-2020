function proportionIntroduced() {

    (function(H) {
        H.wrap(H.Tooltip.prototype, "refresh", function(c, p, e) {
            if(this.options.reversed && this.options.shared) {
                p.reverse();
            }
            c.call(this, p, e);
        });
    })(Highcharts);

    var chart = Highcharts.chart('proportion-introduced', {
        chart: {
            type: 'bar',
            zoomType: 'xy',
            //spacingTop: 100,
            backgroundColor: 'transparent',
            marginRight: 50, // Room for the stacked data labels
            style: {
                fontFamily: 'Montserrat'
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },

        xAxis: [{
            categories: [
                "АФРИКА",
                "АЗИЯ",
                "ЕВРОПА",
                "СЕВЕРНАЯ И ЦЕНТРАЛЬНАЯ АМЕРИКА",
                "ОКЕАНИЯ",
                "ЮЖНАЯ АМЕРИКА",
                "ВЕСЬ МИР"],
            gridLineWidth: 0,
            tickLength: 0,
            //lineWidth: 0,
            lineColor: '#D6D6D6',
            gridLineColor: "transparent",
            labels: {
                style: {
                    fontWeight: 600,
                    color: '#999999',
                    fontSize: '11px',
                    textTransform: 'uppercase',
                    textAlign: 'right'
                },
                useHTML: true
            }
        }],


        yAxis: [{
            labels: {
                //enabled: false,
                formatter: function () {
                    return this.value + "%";
                }
            },
            gridLineColor: "#D6D6D6",

            //enabled: false,
            // gridLineWidth: 0,
            // //lineWidth: 0,
            // tickLength: 0,
            // tickWidth: 0,
            stackLabels: {
                style: {
                    color: '#ffffff',
                    textOutline: "0",
                    fontWeight: "bold"
                },
                enabled: false,
                allowOverlap: true,
                align: 'right',
                x: -50

            },

            title: {
                enabled: false
            }
        }],

        tooltip: {
            shared: true,
            useHTML: true,
            hideDelay: 500,
            backgroundColor: null,
            borderWidth: 0,
            shadow: false,
            reversed: true,
            valueSuffix: '%',
            // pointFormat: "{series.name}: {point.y} ('000 ha)<br>",
            style: {
                padding: 0,
                //width: 250
            }
        },

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderColor: '#f7f7f7'
            },
            bar: {
                stacking: '%',
                // dataLabels: {
                //     enabled: true,
                //     crop: false,
                //     overflow: 'none'
                // }
                // dataLabels: {
                //     enabled: true,
                //     inside: true,
                //     align: "center",
                //     verticalAlign: "bottom",
                //     color: "#FFFFFF",
                //     style: {"fontSize": "12px", "fontWeight": "normal", "textOutline": "0"}
                // }

            }

        },

        series: [{
            name: 'Интродуцированные породы',
            data: [70,
                32,
                78,
                4,
                78,
                97,
                44],
            color: "#99bb35",
            index: 4
        },{
            name: 'Местные породы',
            data: [30,
                68,
                22,
                96,
                22,
                3,
                56],
            color: "#51997c",
            index: 3
        }]
    });


}



