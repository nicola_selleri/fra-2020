


$('document').ready(function(){




    $('.f-box-1').css('width', '74%');
    $('.f-box-1 .box-color').css('background-color', '#009d6f');

    $('.f-box-2').css('width', '8%');
    $('.f-box-2 .box-color').css('background-color', '#fa7400');

    $('.f-box-3').css('width', '6%');
    $('.f-box-3 .box-color').css('background-color', '#fa7400');

    $('.f-box-4').css('width', '6%');
    $('.f-box-4 .box-color').css('background-color', '#fa7400');

    $('.f-box-5').css('width', '6%');
    $('.f-box-5 .box-color').css('background-color', '#fa7400');





    $('#range-framework').rangeslider({

        // Feature detection the default is `true`.
        // Set this to `false` if you want to use
        // the polyfill also in Browsers which support
        // the native <input type="range"> element.
        polyfill: false,

// Callback function
        onSlide: function(position, value) {

            $('.f-box-1').removeClass('active');
            $('.f-box-2').removeClass('active');
            $('.f-box-3').removeClass('active');
            $('.f-box-4').removeClass('active');
            $('.f-box-5').removeClass('active');


        },

        // Callback function
        onSlideEnd: function(position, value) {



            $('.conceptual-f-container .rangeslider__handle').addClass('range-moved');

            $('.f-box-1').addClass('active');
            $('.f-box-2').addClass('active');
            $('.f-box-3').addClass('active');
            $('.f-box-4').addClass('active');
            $('.f-box-5').addClass('active');


            $('.f-box-1').removeClass('stat-3');
            $('.f-box-1').removeClass('stat-4');
            $('.f-box-2').removeClass('stat-4');

            switch(value) {
                case 0:
                    $('.conc-f-map-a').css('width', '100%');
                    $('.conc-f-map-b').css('width', '0%');
                    $('.conc-f-map-c').css('width', '0%');
                    $('.conc-f-map-d').css('width', '0%');
                    $('.conc-f-map-e').css('width', '0%');

                    $('.framework-phases').text('INTENDED USE');


                    $('.f-box-1').css('width', '74%');
                    $('.f-box-1 .box-color').css('background-color', '#009d6f');
                    $('.f-box-1 .label').text('INTENDED FOR FOOD');


                    $('.f-box-2').css({'width': '8%','opacity': 1});
                    $('.f-box-2 .box-color').css('background-color', '#fa7400');
                    $('.f-box-2 .label').text('FEED');

                    $('.f-box-3').css({'width': '6%','opacity': 1});
                    $('.f-box-3 .box-color').css('background-color', '#fa7400');
                    $('.f-box-3 .label').text('SEED');

                    $('.f-box-4').css({'width': '6%','opacity': 1});
                    $('.f-box-4 .box-color').css('background-color', '#fa7400');
                    $('.f-box-4 .label').text('INDUSTRIAL USE');

                    $('.f-box-5').css({'width': '6%','opacity': 1});
                    $('.f-box-5 .box-color').css('background-color', '#fa7400');
                    $('.f-box-5 .label').text('OTHER');
                    break;
                case 1:
                    $('.conc-f-map-a').css('width', '75%');
                    $('.conc-f-map-b').css('width', '25%');
                    $('.conc-f-map-c').css('width', '0%');
                    $('.conc-f-map-d').css('width', '0%');
                    $('.conc-f-map-e').css('width', '0%');

                    $('.framework-phases').text('FRAGMENTS');

                    $('.f-box-1').css('width', '75%');
                    $('.f-box-1 .box-color').css('background-color', '#009d6f');
                    $('.f-box-1 .label').text('FOOD');

                    $('.f-box-2').css({'width': '10%','opacity': 1});
                    $('.f-box-2 .box-color').css('background-color', '#009d6f');
                    $('.f-box-2 .label').text('INEDIBLE PARTS');

                    $('.f-box-3').css({'width': '8%','opacity': 1});
                    $('.f-box-3 .box-color').css('background-color', '#fa7400');
                    $('.f-box-3 .label').text('FEED');

                    $('.f-box-4').css({'width': '8%','opacity': 1});
                    $('.f-box-4 .box-color').css('background-color', '#fa7400');
                    $('.f-box-4 .label').text('OTHER');

                    $('.f-box-5').css({'width': '0%','opacity': 0});
                    $('.f-box-5 .box-color').css('background-color', 'transparent');
                    break;
                case 2:
                    $('.conc-f-map-a').css('width', '56%');
                    $('.conc-f-map-b').css('width', '25%');
                    $('.conc-f-map-c').css('width', '11%');
                    $('.conc-f-map-d').css('width', '8%');
                    $('.conc-f-map-e').css('width', '0%');

                    $('.framework-phases').text('DESTINATION');

                    $('.f-box-1').css('width', '54%');
                    $('.f-box-1 .box-color').css('background-color', '#009d6f');
                    $('.f-box-1 .label').text('FOOD WITH OR WITHOUT QUALITATIVE LOSS AND WASTE');
                    $('.f-box-1').addClass('stat-3');

                    $('.f-box-2').css('width', '46%');
                    $('.f-box-2 .box-color').css('background-color', '#c22e0c');
                    $('.f-box-2 .label').text('FOOD LOSS AND WASTE Quantitative');

                    $('.f-box-3').css({'width': '0%','opacity': 0});
                    $('.f-box-3 .box-color').css('background-color', 'transparent');

                    $('.f-box-4').css({'width': '0%','opacity': 0});
                    $('.f-box-4 .box-color').css('background-color', 'transparent');

                    $('.f-box-5').css({'width': '0%','opacity': 0});
                    $('.f-box-5 .box-color').css('background-color', 'transparent');
                    break;
                case 3:
                    $('.conc-f-map-a').css('width', '30%');
                    $('.conc-f-map-b').css('width', '25%');
                    $('.conc-f-map-c').css('width', '11%');
                    $('.conc-f-map-d').css('width', '8%');
                    $('.conc-f-map-e').css('width', '26%');

                    $('.framework-phases').text('DESTINATION');


                    $('.f-box-1').css('width', '25%');
                    $('.f-box-1 .box-color').css('background-color', '#009d6f');
                    $('.f-box-1 .label').text('EATEN BY PEOPLE');
                    $('.f-box-1').addClass('stat-4');


                    $('.f-box-2').css('width', '29%');
                    $('.f-box-2 .box-color').css('background-color', '#fa7400');
                    $('.f-box-2 .label').text('ECONOMICALLY PRODUCTIVE USE');
                    $('.f-box-2').addClass('stat-4');

                    $('.f-box-3').css({'width': '46%','opacity': 1});
                    $('.f-box-3 .box-color').css('background-color', '#c22e0c');
                    $('.f-box-3 .label').text('FOOD LOSS AND WASTE MANAGEMENT');

                    $('.f-box-4').css({'width': '0%','opacity': 0});
                    $('.f-box-4 .box-color').css('background-color', 'transparent');

                    $('.f-box-5').css({'width': '0%','opacity': 0});
                    $('.f-box-5 .box-color').css('background-color', 'transparent');
                    break;


            }

        },


    });


});


