function annualForest() {

    (function (H) {
        H.wrap(H.Tooltip.prototype, "refresh", function (c, p, e) {
            if (this.options.reversed && this.options.shared) {
                p.reverse();
            }
            c.call(this, p, e);
        });
    })(Highcharts);

    var chart = Highcharts.chart('annual-forest', {
        chart: {
            type: 'column',
            zoomType: 'xy',
            //spacingTop: 100,
            backgroundColor: 'transparent',
            marginRight: 50, // Room for the stacked data labels
            style: {
                fontFamily: 'Montserrat'
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },

        xAxis: [{
            categories: [
                "1990–2000",
                "2000–2010",
                "2010–2020"
            ],
            gridLineWidth: 0,
            tickLength: 0,
            //lineWidth: 0,
            lineColor: '#D6D6D6',
            gridLineColor: "transparent",
            labels: {
                style: {
                    fontWeight: 600,
                    color: '#999999',
                    fontSize: '11px',
                    textTransform: 'uppercase',
                    textAlign: 'right'
                },
                useHTML: true
            }
        }],


        yAxis: [{
            labels: {
                //enabled: false,
                formatter: function () {
                    return this.value / 1000;
                }
            },
            gridLineColor: "#D6D6D6",
            max: 1000,
            plotLines: [{
                color: '#d5d5d5',
                width: 2,
                value: 0
            }],
            //enabled: false,
            // gridLineWidth: 0,
            // //lineWidth: 0,
            // tickLength: 0,
            // tickWidth: 0,


            title: {
                // enabled: false,
                text: "百万公顷/年",
                style: {
                    //color: 'red'
                }
            }
        }],

        tooltip: {
            shared: true,
            useHTML: true,
            hideDelay: 500,
            backgroundColor: null,
            borderWidth: 0,
            shadow: false,
            reversed: true,
            // valueSuffix: ' Million ha per year',
            pointFormatter: function () {
                return '<b>' + (this.y / 1000).toFixed(1) + ' 百万公顷/年</b>';
            },
            style: {
                padding: 0,
                //width: 250
            }
        },

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                //borderColor: '#f7f7f7'

            },
            column: {

                shadow: false,
                // dataLabels: {
                //     enabled: true,
                //     crop: false,
                //     overflow: 'none'
                // },
                dataLabels: {
                    enabled: true,
                    inside: true,
                    align: "center",
                    color: "#ffffff",
                    style: {"fontSize": "10px", "fontWeight": "normal", "textOutline": "0"},
                    formatter: function () {
                        return (this.y / 1000).toFixed(1);
                    }
                }
            }

        },

        series: [{
            name: '1990-2020年森林面积年度净变化',
            data: [{y: -7838.390019, color: '#0f8eaa'},
                {y: -5173.257818, color: '#a3ce47'},
                {y: -4738.613403, color: '#22925e'}]


        }]
    });


}



