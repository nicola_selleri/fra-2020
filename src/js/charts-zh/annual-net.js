function annualNet() {

    (function(H) {
        H.wrap(H.Tooltip.prototype, "refresh", function(c, p, e) {
            if(this.options.reversed && this.options.shared) {
                p.reverse();
            }
            c.call(this, p, e);
        });
    })(Highcharts);

    var chart = Highcharts.chart('annual-net', {
        chart: {
            type: 'column',
            zoomType: 'xy',
            //spacingTop: 100,
            backgroundColor: 'transparent',
            marginRight: 50, // Room for the stacked data labels
            style: {
                fontFamily: 'Montserrat'
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },

        xAxis: [{
            categories: [
                "1990–2000",
                "2000–2010",
                "2010–2020"
            ],
            gridLineWidth: 0,
            tickLength: 0,
            //lineWidth: 0,
            lineColor: '#D6D6D6',
            gridLineColor: "transparent",
            labels: {
                style: {
                    fontWeight: 600,
                    color: '#999999',
                    fontSize: '11px',
                    textTransform: 'uppercase',
                    textAlign: 'right'
                },
                useHTML: true
            }
        }],


        yAxis: [{
            labels: {
                //enabled: false,

            },
            gridLineColor: "#D6D6D6",
            plotLines: [{
                color: '#d5d5d5',
                width: 2,
                value: 0
            }],
            //enabled: false,
            // gridLineWidth: 0,
            // //lineWidth: 0,
            // tickLength: 0,
            // tickWidth: 0,


            title: {
                // enabled: false,
                text: "百万公顷/年",
                style: {
                    //color: 'red'
                }
            }
        }],

        tooltip: {
            shared: true,
            useHTML: true,
            hideDelay: 500,
            backgroundColor: null,
            borderWidth: 0,
            shadow: false,
            //reversed: true,
            valueSuffix: '百万公顷/年',
            // pointFormat: "{series.name}: {point.y} ('000 ha)<br>",
            style: {
                padding: 0,
                //width: 250
            },
            formatter: function () {

                return this.points.reduce(function (s, point) {
                    //console.log(point);
                    return s + '<div class="tooltip-point-item"><span class="tooltip-point-color" style="background-color:' + point.color + ';"></span> ' + point.series.name + ':  <b> ' +
                        Math.round(point.y) + '百万公顷/年</b></div>';
                }, '<span class="tooltip-point-item-title">' + this.x + '</span>');
            }
        },

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                //borderColor: '#f7f7f7'

            },
            column: {

                shadow: false,
                // dataLabels: {
                //     enabled: true,
                //     crop: false,
                //     overflow: 'none'
                // },
                dataLabels: {
                    enabled: true,
                    inside: true,
                    align: "center",

                    color: "#ffffff",

                    style: {"fontSize": "10px", "fontWeight": "normal", "textOutline": "0"},
                    formatter: function () {
                        return Math.round(this.y);
                    }
                },

            }





        },

        series: [{
            name: '天然再生林',
            borderColor: "#36723D",
            color: "#36723D",
            data: [-11.93,
                -10.34,
                -7.84]


        }, {
            name: '人工林',
            borderColor: "#9fc436",
            color: "#9fc436",
            data: [4.06,
                5.13,
                3.06
                ],


        }]
    });


}



