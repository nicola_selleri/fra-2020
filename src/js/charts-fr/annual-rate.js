function annualRate() {

    (function(H) {
        H.wrap(H.Tooltip.prototype, "refresh", function(c, p, e) {
            if(this.options.reversed && this.options.shared) {
                p.reverse();
            }
            c.call(this, p, e);
        });
    })(Highcharts);

    var chart = Highcharts.chart('annual-rate', {
        chart: {
            type: 'column',
            zoomType: 'xy',
            //spacingTop: 100,
            backgroundColor: 'transparent',
            marginRight: 50, // Room for the stacked data labels
            style: {
                fontFamily: 'Montserrat'
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },

        xAxis: [{
            categories: [
                "1990–2000",
                "2000–2010",
                "2010–2015",
                "2015–2020"
            ],
            gridLineWidth: 0,
            tickLength: 0,
            //lineWidth: 0,
            lineColor: '#D6D6D6',
            gridLineColor: "transparent",
            labels: {
                style: {
                    fontWeight: 600,
                    color: '#999999',
                    fontSize: '11px',
                    textTransform: 'uppercase',
                    textAlign: 'right'
                },
                useHTML: true
            }
        }],


        yAxis: [{
            labels: {
                //enabled: false,
                formatter: function () {
                    return this.value / 1000;
                }
            },
            gridLineColor: "#D6D6D6",
            plotLines: [{
                color: '#d5d5d5',
                width: 2,
                value: 0
            }],
            //enabled: false,
            // gridLineWidth: 0,
            // //lineWidth: 0,
            // tickLength: 0,
            // tickWidth: 0,


            title: {
                // enabled: false,
                text: "Millions d’hectares par an",
                style: {
                    //color: 'red'
                }
            }
        }],

        tooltip: {
            shared: true,
            useHTML: true,
            hideDelay: 500,
            backgroundColor: null,
            borderWidth: 0,
            shadow: false,
            reversed: true,
            // pointFormat: "{series.name}: {point.y} ('000 ha)<br>",
            style: {
                padding: 0,
                //width: 250
            },
            formatter: function () {

                return this.points.reduce(function (s, point) {
                    return s + '<div class="tooltip-point-item"><span class="tooltip-point-color" style="background-color:' + point.color + ';"></span> ' + point.series.name + ':  <b> ' +
                        Math.round(point.y / 1000) + ' Million ha per year</b></div>';
                }, '<span class="tooltip-point-item-title">' + this.x + '</span>');
            }
        },

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                //borderColor: '#f7f7f7'
                stacking: 'normal'
            },
            column: {

                shadow: false,
                // dataLabels: {
                //     enabled: true,
                //     crop: false,
                //     overflow: 'none'
                // },
                dataLabels: {
                    enabled: true,
                    inside: true,
                    align: "center",

                    color: "#1b1d1f",

                    style: {"fontSize": "10px", "fontWeight": "normal", "textOutline": "0"},
                    formatter: function () {
                        return Math.round(this.y / 1000);
                    }
                }
            }

        },

        series: [{
            name: 'Expansion de la forêt',
            borderColor: "#3aa08a",
            borderWidth: 2,
            color: "#3aa08a",
            dataLabels: {
                color: "#ffffff",
            },
            data: [7901,
                9871,
                7252,
                5183]


        }, {
            name: 'Déforestation',
            borderColor: "#b1a7a5",
            borderWidth: 2,
            color: "#e0e0d9",
            data: [-15822,
                -15132,
                -11812,
                -10159
                // {y:-5249, color: '#e0e0d9'},
                // {y:-3403, color: '#e0e0d9'},
                ],


        }]
    });


}



