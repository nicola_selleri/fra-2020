function proportionForest() {

    (function(H) {
        H.wrap(H.Tooltip.prototype, "refresh", function(c, p, e) {
            if(this.options.reversed && this.options.shared) {
                p.reverse();
            }
            c.call(this, p, e);
        });
    })(Highcharts);

    var chart = Highcharts.chart('proportion-forest', {
        chart: {
            type: 'bar',
            zoomType: 'xy',
            //spacingTop: 100,
            backgroundColor: 'transparent',
            marginRight: 50, // Room for the stacked data labels
            style: {
                fontFamily: 'Montserrat'
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },

        xAxis: [{
            categories: [
                "Africa",
                "Asia",
                "Europe",
                "North and Central America",
                "Oceania",
                "South America",
                "World"],
            gridLineWidth: 0,
            tickLength: 0,
            //lineWidth: 0,
            lineColor: '#D6D6D6',
            gridLineColor: "transparent",
            labels: {
                style: {
                    fontWeight: 600,
                    color: '#999999',
                    fontSize: '11px',
                    textTransform: 'uppercase',
                    textAlign: 'right'
                },
                useHTML: true
            }
        }],


        yAxis: [{
            labels: {
                //enabled: false,
                formatter: function () {
                    return this.value + "%";
                }
            },
            gridLineColor: "#D6D6D6",

            //enabled: false,
            // gridLineWidth: 0,
            // //lineWidth: 0,
            // tickLength: 0,
            // tickWidth: 0,
            stackLabels: {
                enabled: false,
            },

            title: {
                enabled: false
            }
        }],

        tooltip: {
            shared: true,
            useHTML: true,
            hideDelay: 500,
            backgroundColor: null,
            borderWidth: 0,
            shadow: false,
            reversed: true,
            valueSuffix: '%',
            // pointFormat: "{series.name}: {point.y} ('000 ha)<br>",
            style: {
                padding: 0,
                //width: 250
            }
        },

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderColor: '#f7f7f7'
            },
            bar: {
                // dataLabels: {
                //     enabled: true,
                //     crop: false,
                //     overflow: 'none'
                // }
                // dataLabels: {
                //     enabled: true,
                //     inside: true,
                //     align: "center",
                //     verticalAlign: "bottom",
                //     color: "#FFFFFF",
                //     style: {"fontSize": "12px", "fontWeight": "normal", "textOutline": "0"}
                // }

            }

        },

        series: [{
            name: 'Proportion of forest area in protected areas',
            data: [27,
            25,
            6,
            11,
            16,
            31,
            18],
            color: "#99bb35",
            index: 4
        }]
    });


}



