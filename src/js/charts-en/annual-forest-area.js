function annualForestArea() {

    (function(H) {
        H.wrap(H.Tooltip.prototype, "refresh", function(c, p, e) {
            if(this.options.reversed && this.options.shared) {
                p.reverse();
            }
            c.call(this, p, e);
        });
    })(Highcharts);

    var chart = Highcharts.chart('annual-forest-area', {
        chart: {
            type: 'column',
            zoomType: 'xy',
            //spacingTop: 100,
            backgroundColor: 'transparent',
            marginRight: 50, // Room for the stacked data labels
            style: {
                fontFamily: 'Montserrat'
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },

        xAxis: [{
            categories: [
                "Asia",
                "Oceania",
                "Europe",
                "North and Central America",
                "South America",
                "Africa"
            ],
            gridLineWidth: 0,
            tickLength: 0,
            //lineWidth: 0,
            lineColor: '#D6D6D6',
            gridLineColor: "transparent",
            labels: {
                style: {
                    fontWeight: 600,
                    color: '#999999',
                    fontSize: '11px',
                    textTransform: 'uppercase',
                    textAlign: 'right'
                },
                useHTML: true
            }
        }],


        yAxis: [{
            labels: {
                //enabled: false,

                formatter: function () {
                    return this.value / 1000;
                }
            },
            gridLineColor: "#D6D6D6",
            plotLines: [{
                color: '#d5d5d5',
                width: 2,
                value: 0
            }],
            //enabled: false,
            // gridLineWidth: 0,
            // //lineWidth: 0,
            // tickLength: 0,
            // tickWidth: 0,


            title: {
                // enabled: false,
                text: "Million ha per year",
                style: {
                    //color: 'red'
                }
            }
        }],

        tooltip: {
            shared: true,
            useHTML: true,
            // hideDelay: 50000,
            backgroundColor: null,
            borderWidth: 0,
            shadow: false,
            reversed: true,
            // pointFormat: "{series.name}: {point.y} ('000 ha)<br>",
            style: {
                padding: 0,
                //width: 250
            },
            positioner: function () {
                return { x: (this.chart.plotSizeX/2), y: this.chart.plotSizeY };
            },
            formatter: function () {

                return this.points.reduce(function (s, point) {
                    return s + '<div class="tooltip-point-item"><span class="tooltip-point-color" style="background-color:' + point.color + ';"></span> ' + point.series.name + ':  <b> ' +
                        (point.y / 1000).toFixed(1) + ' Million ha per year</b></div>';
                }, '<span class="tooltip-point-item-title">' + this.x + '</span>');
            }

        },

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                //borderColor: '#f7f7f7'
            },
            column: {

                shadow: false,
                // dataLabels: {
                //     enabled: true,
                //     crop: false,
                //     overflow: 'none'
                // },
                dataLabels: {
                    enabled: true,
                    inside: true,
                    align: "center",
                    verticalAlign: "top",
                    color: "#1b1d1f",
                    y: -30,
                    style: {"fontSize": "10px", "fontWeight": "normal", "textOutline": "0"},
                    formatter: function () {
                        return (this.y / 1000).toFixed(1);
                    }
                }
            }

        },

        series: [{
            name: '1990–2000',
            borderColor: "#0087a6",
            color: "#0087a6",
            data: [202,
            {y:-165, color: '#e0e0d9'},
                795,
                {y:-293, color: '#e0e0d9'},
                {y:-5102, color: '#e0e0d9'},
                {y:-3275, color: '#e0e0d9'},
                ]


        }, {
            name: '2000–2010',
            borderColor: "#9ccb3a",
            color: "#9ccb3a",
            data: [2355,
                 {y:-231, color: '#e0e0d9'},
                1171,
                184,
                {y:-5249, color: '#e0e0d9'},
                {y:-3403, color: '#e0e0d9'},
                ],


        }, {
            name: '2010–2020',
            borderColor: "#00854a",
            color: "#00854a",
            data: [1173,
                423,
                348,
                {y:-148, color: '#e0e0d9'},
                {y:-2597, color: '#e0e0d9'},
                {y:-3938, color: '#e0e0d9'},
            ]


        }]
    });


}



