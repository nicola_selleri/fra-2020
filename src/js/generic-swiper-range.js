// This in HTML

// <div class="scenarios1-container">
//
//     <!-- Slider main container -->
// <div class="swiper-container scenarios1-swiper">
//     <!-- Additional required wrapper -->
// <div class="swiper-wrapper">
//     <!-- Slides -->
//     <div class="swiper-slide">Slide 1</div>
// <div class="swiper-slide">Slide 2</div>
// <div class="swiper-slide">Slide 3</div>
// </div>
// </div>
//
// <div class="scenarios-range-container">
//     <input type="range" min="0" max="2" step="1" value="0" data-rangeslider id="range-scenarios1">
//     </div>
//
//     </div>


var swiperScen1 = new Swiper ('.scenarios1-swiper', {

})

swiperScen1.on('slideChangeTransitionEnd', function () {



    var currentSlide = swiperScen1.activeIndex;

    switch(currentSlide) {
        case 0:
            $('#range-scenarios1').val(0).change();
            break;
        case 1:
            $('#range-scenarios1').val(1).change();
            break;
        case 2:
            $('#range-scenarios1').val(2).change();
            break;
        
    }

});

swiperScen1.on('slideChangeTransitionStart', function () {


    var currentSlide = swiperScen1.activeIndex;

    switch(currentSlide) {
        case 0:
            // $('.triangle-marker').css('left','calc(10% - 10px)');
            break;
        case 1:
            // $('.triangle-marker').css('left','calc(30% - 10px)');
            break;
        case 2:

            // $('.triangle-marker').css('left','calc(50% - 10px)');
            break;

    }

});




$('#range-scenarios1').rangeslider({

    // Feature detection the default is `true`.
    // Set this to `false` if you want to use
    // the polyfill also in Browsers which support
    // the native <input type="range"> element.
    polyfill: false,





    // Callback function
    onSlideEnd: function(position, value) {

        switch(value) {
            case 0:
                swiperScen1.slideTo(0);
                // $('.triangle-marker').css('left','calc(10% - 10px)');
                break;
            case 1:
                swiperScen1.slideTo(1);
                // $('.triangle-marker').css('left','calc(30% - 10px)');
                break;
            case 2:
                swiperScen1.slideTo(2);
                // $('.triangle-marker').css('left','calc(50% - 10px)');
                break;


        }

    },


});
