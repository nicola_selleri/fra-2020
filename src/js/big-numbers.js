// Big Numbers

var options = {
    //useEasing: true,
    // suffix: '%'
};



var optionsP = {
    //useEasing: true,
    //  suffix: 'ha',
      separator: ' '
};

var options2 = {
     //useEasing: true,
      suffix: '%'
};





var p1Container = document.getElementById('p1');
var p1 = new CountUp(p1Container, 0, 815, 0, 2, optionsP);

var p2Container = document.getElementById('p2');
var p2 = new CountUp(p2Container, 0, 497, 0, 2, optionsP);

var p3Container = document.getElementById('p3');
var p3 = new CountUp(p3Container, 0, 347, 0, 2, optionsP);

var p4Container = document.getElementById('p4');
var p4 = new CountUp(p4Container, 0, 310, 0, 2, optionsP);

var p5Container = document.getElementById('p5');
var p5 = new CountUp(p5Container, 0, 220, 0, 2, optionsP);

var p6Container = document.getElementById('p6');
var p6 = new CountUp(p6Container, 0, 1870, 0, 2, optionsP);


/// Percentages

var p1bContainer = document.getElementById('p1b');
var p1b = new CountUp(p1bContainer, 0, 20, 0, 2, options2);

var p2bContainer = document.getElementById('p2b');
var p2b = new CountUp(p2bContainer, 0, 12, 0, 2, options2);

var p3bContainer = document.getElementById('p3b');
var p3b = new CountUp(p3bContainer, 0, 9, 0, 2, options2);

var p4bContainer = document.getElementById('p4b');
var p4b = new CountUp(p4bContainer, 0, 8, 0, 2, options2);

var p5bContainer = document.getElementById('p5b');
var p5b = new CountUp(p5bContainer, 0, 5, 0, 2, options2);

var p6bContainer = document.getElementById('p6b');
var p6b = new CountUp(p6bContainer, 0, 46, 0, 2, options2);