

function showResults(currentQuestion, questionID, correctAnswer) {
    var currentContainer = document.getElementById(currentQuestion);
    var statementContainer = $(currentContainer).find(".statement");
    var answerContainers = currentContainer.querySelectorAll(".answers");

    // console.log(currentContainer);
     console.log(statementContainer);

    var answerContainer = answerContainers[0];

    // find selected answer
    var selector = "input[name=question" + questionID + "]:checked";
    var correctItem = "label." + correctAnswer + "-" + questionID;
    var userAnswer = (answerContainer.querySelector(selector) || {}).value; // Viene a, b, c
    var toShowAnswer = answerContainer.querySelector(correctItem) || {};

    //console.log(selector);
    // console.log(toShowAnswer);

    // Version without bootrap
    // statementContainer[0].classList.add('show-statement');

    // Version with bootstrap


    // if answer is correct
    if (userAnswer === correctAnswer) {
        currentContainer.classList.add('correct');
        // add to the number of correct answers
        toShowAnswer.classList.add('correct');
        statementContainer.collapse('show');
        //console.log('ahe');
    } else {

        currentContainer.classList.add('wrong');
        // if answer is wrong or blank
        toShowAnswer.classList.add('wrong');
        statementContainer.collapse('show');
    }



}