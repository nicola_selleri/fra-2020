// function showSubC(e){
//
//     $(e).parent().find('.forest-types-panel').toggleClass('open')
//
// }


function init() {


    var coverVideo = document.getElementById("video-cover");
    var natureIcoVideo = document.getElementById("natural-ico");
    var plantedIcoVideo = document.getElementById("planted-ico");

    var forestOwn1 = document.getElementById("forest-own-item-1");
    var forestOwn2 = document.getElementById("forest-own-item-2");
    var forestOwn3 = document.getElementById("forest-own-item-3");
    var forestOwn4 = document.getElementById("forest-own-item-4");
    var forestOwn5 = document.getElementById("forest-own-item-5");
    var forestOwn6 = document.getElementById("forest-own-item-6");
    var forestOwn7 = document.getElementById("forest-own-item-7");

    var areasTropical = document.getElementById("areas-ico-tropical");
    var areasBoreal = document.getElementById("areas-ico-boreal");
    var areasTemperate = document.getElementById("areas-ico-temperate");
    var areasSubtropical = document.getElementById("areas-ico-subtropical");

    var hectareVideo = document.getElementById("hectare-video");


    // function checkforVideo() {
    //     //Every 500ms, check if the video element has loaded
    //     var b = setInterval(function () {
    //         if (coverVideo.readyState >= 3) {
    //             //This block of code is triggered when the video is loaded
    //
    //             console.log('video ready');
    //
    //             //your code goes here
    //
    //
    //             //stop checking every half second
    //             clearInterval(b);
    //
    //         }
    //     }, 500);
    // }
    //
    // checkforVideo();

    $('body').removeClass('loading');

    /// FOREST VIDEOS

    function forestVideosIn() {
        setTimeout(function () {
            forestOwn1.play();
        }, 0);
        setTimeout(function () {
            forestOwn2.play();
        }, 100);
        setTimeout(function () {
            forestOwn3.play();
        }, 200);
        setTimeout(function () {
            forestOwn4.play();
        }, 300);
        setTimeout(function () {
            forestOwn5.play();
        }, 400);
        setTimeout(function () {
            forestOwn6.play();
        }, 500);
        setTimeout(function () {
            forestOwn7.play();
        }, 600);
    }


    function forestVideosReset() {

        forestOwn1.pause();
        forestOwn1.currentTime = 0;
        forestOwn2.pause();
        forestOwn2.currentTime = 0;
        forestOwn3.pause();
        forestOwn3.currentTime = 0;
        forestOwn4.pause();
        forestOwn4.currentTime = 0;
        forestOwn5.pause();
        forestOwn5.currentTime = 0;
        forestOwn6.pause();
        forestOwn6.currentTime = 0;
        forestOwn7.pause();
        forestOwn7.currentTime = 0;
    }


    /// AREAS VIDEO

    var borealLayer = $('.areas-layer.boreal');
    var temperateLayer = $('.areas-layer.temperate');
    var subtropicalLayer = $('.areas-layer.subtropical');
    var tropicalLayer = $('.areas-layer.tropical');

    var borealSwitch = $('.areas-switch.boreal');
    var temperateSwitch = $('.areas-switch.temperate');
    var subtropicalSwitch = $('.areas-switch.subtropical');
    var tropicalSwitch = $('.areas-switch.tropical');


    function areasVideosIn() {

        // Layers animations

        setTimeout(function () {
            borealLayer.addClass('in');
        }, 0);

        setTimeout(function () {
            temperateLayer.addClass('in');
        }, 100);

        setTimeout(function () {
            subtropicalLayer.addClass('in');
        }, 200);

        setTimeout(function () {
            tropicalLayer.addClass('in');
        }, 300);


        // Switches animations

        setTimeout(function () {
            areasTropical.play();
        }, 0);
        setTimeout(function () {
            areasBoreal.play();
        }, 200);
        setTimeout(function () {
            areasTemperate.play();
        }, 400);
        setTimeout(function () {
            areasSubtropical.play();
        }, 600);

    }

    function hideAreasLayers(){
        borealLayer.addClass('hide');
        temperateLayer.addClass('hide');
        subtropicalLayer.addClass('hide');
        tropicalLayer.addClass('hide');
    }

    function showAreasLayers() {
        borealLayer.removeClass('hide');
        temperateLayer.removeClass('hide');
        subtropicalLayer.removeClass('hide');
        tropicalLayer.removeClass('hide');
    }



    var areaselected;


    borealSwitch.on('click', function (e) {
        toggleAreasLayer(e)
    });
    temperateSwitch.on('click', function (e) {
        toggleAreasLayer(e)
    });
    subtropicalSwitch.on('click', function (e) {
        toggleAreasLayer(e)
    });
    tropicalSwitch.on('click', function (e) {
        toggleAreasLayer(e)
    });

    function toggleAreasLayer(e){

        console.log(e);

        if(e.currentTarget.classList[1] == areaselected){
            showAreasLayers();
            areaselected = "";
        }else{

            switch (e.currentTarget.classList[1]) {

                case "boreal":

                    hideAreasLayers();
                    borealLayer.removeClass('hide');
                    areaselected = e.currentTarget.classList[1];

                    break;

                case "temperate":

                    hideAreasLayers();
                    temperateLayer.removeClass('hide');
                    areaselected = e.currentTarget.classList[1];

                    break;

                case "subtropical":

                    hideAreasLayers();
                    subtropicalLayer.removeClass('hide');
                    areaselected = e.currentTarget.classList[1];

                    break;

                case "tropical":

                    hideAreasLayers();
                    tropicalLayer.removeClass('hide');
                    areaselected = e.currentTarget.classList[1];

                    break;




            }

        }


    }

    // tropicalSwitch.on('click mouseover', function () {
    //     if(areaselected != 1){
    //         areaselected = 1;
    //
    //         hideAreasLayers();
    //         tropicalLayer.removeClass('hide');
    //
    //
    //     }else{
    //         areaselected = 0;
    //         showAreasLayers();
    //         resetAreasSwitches();
    //     }
    //
    // });
    //
    // borealSwitch.on('click mouseover', function () {
    //     if(areaselected != 2){
    //         areaselected = 2;
    //
    //         hideAreasLayers();
    //         borealLayer.removeClass('hide');
    //
    //
    //     }else{
    //         areaselected = 0;
    //         showAreasLayers();
    //         resetAreasSwitches();
    //     }
    //
    //
    // });
    //
    // temperateSwitch.on('click mouseover', function () {
    //     if(areaselected != 3){
    //         areaselected = 3;
    //
    //         hideAreasLayers();
    //         temperateLayer.removeClass('hide');
    //
    //
    //     }else{
    //         areaselected = 0;
    //         showAreasLayers();
    //         resetAreasSwitches();
    //     }
    //
    //
    // });
    //
    // subtropicalSwitch.on('click mouseover', function () {
    //     if(areaselected != 4){
    //         areaselected = 4;
    //
    //         hideAreasLayers();
    //         subtropicalLayer.removeClass('hide');
    //
    //
    //     }else{
    //         areaselected = 0;
    //         showAreasLayers();
    //         resetAreasSwitches();
    //     }
    //
    // });
    //
    //
    //
    //
    //
    //
    // tropicalSwitch.on('mouseleave', function () {
    //     areaselected = 0;
    //     showAreasLayers();
    //
    //
    // });
    //
    // borealSwitch.on('mouseleave', function () {
    //     areaselected = 0;
    //
    //     showAreasLayers();
    //
    //
    // });
    //
    // temperateSwitch.on('mouseleave', function () {
    //     areaselected = 0;
    //     showAreasLayers();
    //
    //
    // });
    //
    // subtropicalSwitch.on('mouseleave', function () {
    //     areaselected = 0;
    //     showAreasLayers();
    //
    // });


    /// SCROLLSTORY

    function setupProse() {
        var height = window.innerHeight; // Can be also half (* 0.5)
        $('.particle-story-a-holder .trigger').css('height', height + 'px');
        //$('.dragged-container').css('height', height + 'px');


    }


    setupProse()

    $(function () {
        // select elements using jQuery since it is a dependency
        var $graphicEl = $('#story-container');

        // viewport height
        var viewportHeight = window.innerHeight;
        var halfViewportHeight = Math.floor(viewportHeight / 2);


        function initScrollStory() {

            // handle the fixed/static position of graphic
            var toggle = function (fixed, bottom) {
                if (fixed) $('#particle-container').addClass('is-fixed')
                else $('#particle-container').removeClass('is-fixed')

                if (bottom) $('#particle-container').addClass('is-bottom')
                else $('#particle-container').removeClass('is-bottom')
            }

            // // handle the fixed/static position of graphic
            // var toggleB = function (fixed, bottom) {
            //     if (fixed) $('#particle-container-b').addClass('is-fixed')
            //     else $('#particle-container-b').removeClass('is-fixed')
            //
            //     if (bottom) $('#particle-container-b').addClass('is-bottom')
            //     else $('#particle-container-b').removeClass('is-bottom')
            // }


            // callback on scrollStory scroll event
            // toggle fixed position
            var handleContainerScroll = function (event) {
                var bottom = false
                var fixed = false

                var bb = $('.particle-story-a-holder')[0].getBoundingClientRect();
                var bottomFromTop = bb.bottom - viewportHeight;

                // console.log(bottomFromTop);
                // console.log('vh: ' + viewportHeight);
                // console.log('bb: ' + bb.bottom);

                if (bb.top < 0 && bottomFromTop > 0) {
                    bottom = false
                    fixed = true
                } else if (bb.top < 0 && bottomFromTop < 0) {
                    bottom = true
                    fixed = false
                }

                toggle(fixed, bottom)
            }


            // instantiate scrollStory
            $graphicEl.scrollStory({
                contentSelector: '.trigger',
                triggerOffset: halfViewportHeight,
                scrollSensitivity: 1,
                containerscroll: handleContainerScroll,
                //debug: true,
                setup: function () {
                    setTimeout(function () {
                        $('.cover-trigger').addClass('in');
                    }, 200);

                },
                itementerviewport: function (ev, item) {
                    //console.log('vai');


                    switch (item.data.name) {

                        case 'video-cover':
                             coverVideo.play();

                            break;


                        case 'forest-types':

                            natureIcoVideo.play();
                            plantedIcoVideo.play();


                            break;

                        case 'step-0':
                            console.log('here');
                            resetTAnim1Step1();
                            $('.percentage-value').attr('class', 'percentage-value');
                            break;

                        case 'step-1':
                            p1.start();
                            p1b.start();
                            startTAnim1Step1();
                            $('.percentage-value').attr('class', 'percentage-value').addClass('step-1');
                            $('.item-label:nth-child(1)').addClass('in');
                            $('.line-1').addClass('on');
                            break;

                        case 'step-2':
                            p2.start();
                            p2b.start();
                            startTAnim1Step2();
                            $('.percentage-value').attr('class', 'percentage-value').addClass('step-2');
                            $('.item-label:nth-child(2)').addClass('in');
                            $('.line-2').addClass('on');
                            break;

                        case 'step-3':
                            p3.start();
                            p3b.start();
                            startTAnim1Step3();
                            $('.percentage-value').attr('class', 'percentage-value').addClass('step-3');
                            $('.item-label:nth-child(3)').addClass('in');
                            $('.line-3').addClass('on');
                            break;

                        case 'step-4':
                            p4.start();
                            p4b.start();
                            startTAnim1Step4();
                            $('.percentage-value').attr('class', 'percentage-value').addClass('step-4');
                            $('.item-label:nth-child(4)').addClass('in');
                            $('.line-4').addClass('on');
                            break;

                        case 'step-5':
                            p5.start();
                            p5b.start();
                            startTAnim1Step5();
                            $('.percentage-value').attr('class', 'percentage-value').addClass('step-5');
                            $('.item-label:nth-child(5)').addClass('in');
                            $('.line-5').addClass('on');
                            break;

                        case 'step-6':
                            p6.start();
                            p6b.start();
                            startTAnim1Step6();
                            $('.percentage-value').attr('class', 'percentage-value').addClass('step-6');
                            $('.item-label:nth-child(6)').addClass('in');
                            $('.rest-of-color').addClass('on');
                            break;


                    }

                },
                itemexitviewport: function (ev, item) {

                    switch (item.data.name) {

                        case 'video-cover':
                            console.log('cover paused');

                             coverVideo.pause();

                            $('#wrapper-scroll-cat').addClass('out');

                            break;


                        case 'forest-types':

                            natureIcoVideo.pause();
                            natureIcoVideo.currentTime = 0;
                            plantedIcoVideo.pause();
                            plantedIcoVideo.currentTime = 0;


                            break;

                        case 'forest-ownership':

                            // forestVideosReset();

                            break;


                    }

                },
                itemfocus: function (ev, item) {

                    //console.log(item);

                    this.filter(item); // This does the trick. Makes the item "invisible" to scrollstory after the first view

                    switch (item.data.name) {

                        case 'annual-forest':
                            annualForest();
                            break;

                        case 'annual-forest-area':
                            annualForestArea();
                            break;

                        case 'annual-net':
                            annualNet();
                            break;

                        case 'annual-rate':
                            annualRate();
                            break;

                        case 'proportion-forest':
                            proportionForest();
                            break;

                        case 'proportion-introduced':
                            proportionIntroduced();
                            break;

                        case 'forest-ownership':

                            forestVideosIn();

                            break;

                        case 'proportion-of-global':

                            areasVideosIn();

                            break;


                        case 'hectare-anim':

                            hectareVideo.play();

                            break;




                    }

                },
                itemfilter: function (ev, item) {

                }
            })

        }

        initScrollStory();
    });


}


$('document').ready(init());


